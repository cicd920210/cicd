package com.isi.cicidprojet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicidprojetApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicidprojetApplication.class, args);
	}

}
