FROM openjdk:17-jdk-slim

LABEL maintainer="Bassiriki Mangane"

EXPOSE 8080

ADD target/cicidprojet-0.0.1-SNAPSHOT.jar cicidprojet-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java", "-jar", "cicidprojet-0.0.1-SNAPSHOT.jar"]
